import React, { Component } from 'react';
import './App.css';
import forkImage from './component/assets/fork.png';
import teaImage from './component/assets/tea.png';
import MenuPrice from './component/Menu/Menu';
import Score from './component/Score/Score';
import Price from './component/Menu/Price';

class App extends Component {
  state = {
    menu: [
        {name: 'Hamburger', count: 0, price: 80, image: forkImage, show: false},
        {name: 'Cheesburger', count: 0, price: 90, image: forkImage, show: false},
        {name: 'Fries', count: 0, price: 45, image: forkImage, show: false},
        {name: 'Coffee', count: 0, price: 70, image: teaImage, show: false},
        {name: 'Tea', count: 0, price: 50, image: teaImage, show: false},
        {name: 'Cola', count: 0, price: 80, image: teaImage, show: false},
    ],
      total: 0,
  };

    addOrder = (name) => {


        const order = [...this.state.menu];

        let copyTotal = this.state.total;

        for(let i = 0; i < order.length; i++) {
            if(order[i].name === name) {
                order[i].count++;

                copyTotal += order[i].price

                order[i].show = true

            }
        }

        this.setState({order: order, total: copyTotal});

    };
    removeComponent = (name) => {
        const copyForRemove = [...this.state.order];
        let copyTotal = this.state.total;

        for(let i = 0; i < copyForRemove.length; i++) {
            if(copyForRemove[i].name === name) {
                if (copyForRemove[i].count !== 0) {
                    copyForRemove[i].count--;
                    copyTotal -= copyForRemove[i].price;
                    if (copyForRemove[i].count < 1){
                        copyForRemove[i].show = false
                    }
                }
            }
        }
        this.setState({order: copyForRemove, total: copyTotal})
    };

  render() {
    return (
      <div className="App">
        <div className="Price">
            <h2 className="Menu">Меню</h2>
          <div className="Prices">
              <MenuPrice
                  onClickAdd={(name) => this.addOrder(name)}
                  order={this.state.menu}
                  remove={this.removeComponent}
              />
          </div>
        </div>
        <div className="Score-order">
          <h2 className="Ord">Order</h2>
          <Price
              onClickAdd={(name) => this.addOrder(name)}
              order={this.state.menu}
              remove={this.removeComponent}
          />
            <Score order={this.state.menu} total={this.state.total}/>
        </div>
      </div>
    );
  }
}

export default App;
