import React, {Fragment} from 'react'

const Orders = (props) => {

    const getOrdersName = () => {
        const names = [];
        for(let i = 0; i < props.count; i++) {
            names.push(props.name)
        }
        return names
    };


    return (
        <Fragment>
            {getOrdersName().map((orderName, index) => {
                return (
                    <div className={`${orderName}`} key={index}>

                    </div>
                )
            })}
        </Fragment>
    )
};

export default Orders;