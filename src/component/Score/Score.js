import React from 'react';
import Orders from "./Order";

const Score = (props) => {
    return (
        <div className="Score">
            {props.order.map((order, index) => {
                return (
                    <Orders key={index} name={order.name} count={order.count}/>
                )
            })}
            <div className="Total-score">
                <span className="Total-price">Total: {props.total} KGS</span>
            </div>
        </div>
    )
};

export default Score;