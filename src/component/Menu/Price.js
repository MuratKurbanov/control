import React from 'react';

const Price = (props) => {
    return (
        props.order.map((fill, key) => {

            return (
                <div className="add-menu" key={key}>
                    <div className="textes">
                        <p className="scr-name">{fill.name}</p>
                        <span className="counter">x {fill.count}</span>
                        <span className="prices">{fill.price} KGS</span>
                    </div>


                    {fill.show ? <button className="delete" onClick={() => props.remove(fill.name)}>X</button> : null}
                </div>
            )
        })
    );
}
export default Price;