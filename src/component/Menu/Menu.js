import React from 'react';

const MenuPrice = (props) => {
    return (
        props.order.map((fill, key) => {

            return (
                <div className="add-ingredient" key={key}>
                    <img src={fill.image} alt="#" className="img" onClick={() => props.onClickAdd(fill.name)}/>
                    <div className="text">
                        <p className="scr-name">{fill.name}</p>
                        <span className="price">Price: {fill.price} KGS</span>
                    </div>
                </div>
            )
        })
    )
};

export default MenuPrice;